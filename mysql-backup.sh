#!/bin/bash
 
################################################################
##
##   MySQL Database Backup Script 
##   Written By: Byron Castro
##
################################################################

TODAY=`date +"%Y-%m-%d"`
NOW=`date`
################################################################
################## Update below values  ########################
################ Configuration variables  ######################

## configuration variables
## set absolute path if you plan set this script in a cron job
DB_BACKUP_PATH='dbbackup'
## set absolute path if you plan set this script in a cron job
LOG_PATH='logs'
## host form connection to the mysql database server
MYSQL_HOST='localhost'
## set the port of the mysql database server
MYSQL_PORT='3306'
## set the user name to connecto the mysql database server
MYSQL_USER='root'
## set the password from the user name to connecto to the mysql database server
MYSQL_PASSWORD='xxxxxx'
## set the databases names in this array
DATABASES=('database_one' 'database_two')

## Create the log file 
log=${LOG_PATH}/log_backup${TODAY}.txt
echo "Backup started at ${NOW}" > $log
#################################################################

## Create the directory to store the backups files
mkdir -p ${DB_BACKUP_PATH}/${TODAY}
 
for database in "${DATABASES[@]}"
do
echo "${NOW} - Backup started for database - ${database}" >> $log
  mysqldump
  ## uncoment the next lines if you recive a error trying to export the databases
   # --no-tablespaces
   # --column-statistics=0
   -h ${MYSQL_HOST} \
   -P ${MYSQL_PORT} \
   -u ${MYSQL_USER} \
   -p${MYSQL_PASSWORD} \
   ${database} | gzip > ${DB_BACKUP_PATH}/${TODAY}/${database}-${TODAY}.sql.gz
done

echo "Backup finish at ${NOW}" >> $log
echo All done
### End of script ####